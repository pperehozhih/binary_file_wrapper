using System;
using System.IO;
using System.ComponentModel;

namespace binary_wrapper
{
	public class BinaryWrapper
	{
		const int item_size = 6;
		Stream 	m_stream;
		bool	m_running;
		long    m_last_best_offset;

		public BinaryWrapper (Stream stream){
			m_stream = stream;
			m_running = false;
			m_last_best_offset = -1;
		}

		public void Execute(){
			lock(m_stream){
				if (m_running)
					return;
				BackgroundWorker bw = new BackgroundWorker ();
				bw.DoWork += Worker;
				bw.RunWorkerAsync ();
				m_running = true;
			}
		}

		void Worker (object sender, DoWorkEventArgs e)
		{
			int size_uint64 = sizeof(UInt64);
			byte[] buff = new byte[size_uint64];
			while (m_running) {
				lock (m_stream) {
					if (m_stream.Read (buff, size_uint64 - item_size, item_size) != item_size) {
						m_running = false;
						continue;
					}
				}
				UInt64 value = BitConverter.ToUInt64 (buff);
				//BitConverter.ToUInt64 ();
			}
		}

		public double Progress(){
			lock(m_stream){
				if (m_stream.Length == 0 && m_running)
					return 0.0;
				return (long)m_stream.Position / (long)m_stream.Length;
			}
		}
	}
}

